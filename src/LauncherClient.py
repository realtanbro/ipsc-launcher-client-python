#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 2012-6-14

@author: lxy@hesong.net
'''

import sys
import os
from ctypes import CDLL, RTLD_GLOBAL, Structure, c_char, c_int, c_void_p, CFUNCTYPE, POINTER, c_char_p, pointer

lib_filename = None
if 'posix' in sys.builtin_module_names:
    lib_filename = 'liblauncher-client.so'
elif 'nt' in sys.builtin_module_names:
    lib_filename = 'launcher-client.dll'

_lib = None

LAUNCHER_CLIENT_ID_SIZE = 256

_cfunc_client_new = None
_cfunc_client_del = None
_cfunc_client_req = None
_cfunc_client_res = None
_cfunc_client_notify = None

class _struct_client(Structure):
    _fields_ = [
        ('process_index', c_int),
        ('project_id', c_char * LAUNCHER_CLIENT_ID_SIZE),
        ('_id', c_char * LAUNCHER_CLIENT_ID_SIZE),
        ('max_recv_size', c_int),
        ('data', c_void_p)
    ]

_prototype_connected_cb = CFUNCTYPE(None, POINTER(_struct_client))
_prototype_disconnected_cb = CFUNCTYPE(None, POINTER(_struct_client))
_prototype_request_cb = CFUNCTYPE(None, POINTER(_struct_client), c_int, c_char_p, c_char_p)
_prototype_response_cb = CFUNCTYPE(None, POINTER(_struct_client), c_int, c_char_p)
_prototype_notify_cb = CFUNCTYPE(None, POINTER(_struct_client), c_char_p, c_char_p)

class _struct_callbacks(Structure):
    _fields_ = [
        ('on_connected', _prototype_connected_cb),
        ('on_disconnected', _prototype_disconnected_cb),
        ('on_request', _prototype_request_cb),
        ('on_response', _prototype_response_cb),
        ('on_notify', _prototype_notify_cb),
    ]

_prototype_client_new = CFUNCTYPE(POINTER(_struct_client), c_int, c_char_p, c_char_p, c_int, POINTER(_struct_callbacks))
_paramflags_client_new = (1, 'process_index', None), (1, 'project_id', None), (1, 'client_id', None), (1, 'max_recv_size', 0), (1, 'callbacks', None)

_prototype_client_del = CFUNCTYPE(None, POINTER(_struct_client))
_paramflags_client_del = (1, 'client', None),

_prototype_client_req = CFUNCTYPE(c_int, POINTER(_struct_client), c_char_p, c_char_p)
_paramflags_client_req = (1, 'client', None), (1, 'flow_id', None), (1, 'content', None)

_prototype_client_res = CFUNCTYPE(c_int, POINTER(_struct_client), c_int, c_char_p)
_paramflags_client_res = (1, 'client', None), (1, 'req_id', None), (1, 'content', None)

_prototype_client_notify = CFUNCTYPE(c_int, POINTER(_struct_client), c_char_p, c_char_p)
_paramflags_client_notify = (1, 'client', None), (1, 'flow_id', None), (1, 'content', None)

def load_lib(filepath):
    global _lib
    if _lib:
        return _lib
    else:
        if 'posix' in sys.builtin_module_names:
            _lib = CDLL(filepath, mode=RTLD_GLOBAL)
        else:
            raise NotImplementedError()
        global _cfunc_client_new, _cfunc_client_del, _cfunc_client_req, _cfunc_client_res, _cfunc_client_notify
        _cfunc_client_new = _prototype_client_new(('lchclt_client_new', _lib), _paramflags_client_new)
        _cfunc_client_del = _prototype_client_del(('lchclt_client_del', _lib), _paramflags_client_del)
        _cfunc_client_req = _prototype_client_req(('lchclt_client_req', _lib), _paramflags_client_req)
        _cfunc_client_res = _prototype_client_res(('lchclt_client_res', _lib), _paramflags_client_res)
        _cfunc_client_notify = _prototype_client_notify(('lchclt_client_notify', _lib), _paramflags_client_notify)        
        return _lib


class Client(object):
    @staticmethod
    def loadLibrary(filepath=None):
        if filepath is None:
            try:
                return load_lib(lib_filename)
            except:
                try:
                    return load_lib(os.path.join(os.path.curdir, lib_filename))
                except:
                    try:
                        return load_lib(os.path.join(os.getcwd(), lib_filename))
                    except:
                        try:
                            return load_lib(os.path.join(os.path.dirname(__file__), lib_filename))
                        except:
                            raise
        else:
            return load_lib(filepath)
        
    def __init__(self, process_index, project_id, client_id, max_recv_size=8192):
        self.__process_index = process_index
        self.__project_id = project_id
        self.__client_id = client_id
        self.__max_recv_size = max_recv_size
        self.__callbacks = _struct_callbacks()
        self.__callbacks.on_connected = _prototype_connected_cb(self.__connected_handler)
        self.__callbacks.on_disconnected = _prototype_disconnected_cb(self.__disconnected_handler)
        self.__callbacks.on_request = _prototype_request_cb(self.__request_handler)
        self.__callbacks.on_response = _prototype_response_cb(self.__response_handler)
        self.__callbacks.on_notify = _prototype_notify_cb(self.__notify_handler)
        self.__client = _cfunc_client_new(process_index, project_id, client_id, max_recv_size, pointer(self.__callbacks))
        assert(self.__client.contents)
        
    def __del__(self):
        _paramflags_client_del(self.__client)
        
    def get_processIndex(self):
        return self.__process_index
    
    processIndex = property(get_processIndex)
    
    def get_projectId(self):
        return self.__project_id
    
    projectId = property(get_projectId)
    
    def get_clientId(self):
        return self.__client_id
    
    clientId = property(get_clientId)
    
    def get_maxRecvSize(self):
        return self.__max_recv_size
    
    maxRecvSize = property(get_maxRecvSize)
        
    def __connected_handler(self, client):
        if hasattr(self, 'onConnected'):
            if callable(self.onConnected):
                self.onConnected()
    
    def __disconnected_handler(self, client):
        if hasattr(self, 'onDisconnected'):
            if callable(self.onDisconnected):
                self.onDisconnected()
    
    def __request_handler(self, client, requestId, funcId, data):
        if hasattr(self, 'onServerRequest'):
            if callable(self.onServerRequest):
                self.onServerRequest(requestId, funcId, data)
    
    def __notify_handler(self, client, funcId, data):
        if hasattr(self, 'onServerNotify'):
            if callable(self.onServerNotify):
                self.onServerNotify(funcId, data)
    
    def __response_handler(self, client, requestId, data):
        if hasattr(self, 'onServerResponse'):
            if callable(self.onServerResponse):
                self.onServerResponse(requestId, data)
    
    def onConnected(self):
        pass
    
    def onDisconnected(self):
        pass
    
    def onServerRequest(self, requestId, funcId, data):
        pass
    
    def onServerNotify(self, funcId, data):
        pass
    
    def onServerResponse(self, requestId, data):
        pass
    
    def request(self, flowId, data):
        return _cfunc_client_req(self.__client, flowId, data)
    
    def notify(self, flowId, data):
        return _cfunc_client_notify(self.__client, flowId, data)
    
    def response(self, requestId, data):
        return _cfunc_client_res(self.__client, requestId, data)

